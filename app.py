from flask import Flask,render_template
from flask import jsonify
import pruebaBD

app = Flask(__name__)
# class Procurador(db.Model):
#     id = db.Column(db.Integer,primary_key=True)
#     procurador = db.Column(db.String(45),nullable=False)
#     legajo = db.Column(db.String(45),nullable=False)
#
#     def __repr__(self):
#         return f"Procurador('{self.procurador}','{self.legajo}')"

procuradores = pruebaBD.selecProcurador()


# exp = pruebaBD.selecExpedientesProcurador(2)

# print(procuradores)


procuradores1 = [
    {
        'nombre': str(procuradores[0][1]),
        'matricula': procuradores[0][2],
        'route': "./expedientes-colom",
    },
    {
        'nombre': str(procuradores[1][1]),
        'matricula': procuradores[1][2],
        'route': "./expedientes-sarg",
    },
    {
        'nombre': str(procuradores[2][1]),
        'matricula': procuradores[2][2],
        'route': "./expedientes-bruna",
    }
]

@app.route('/')
def Index():
    return render_template('index.html',procuradores = procuradores1,expedientes = None)

@app.route('/procuradores')
def Contact():
    return jsonify(procuradores)

@app.route('/ejem')
def Broder():
    return "HOLA"

@app.route('/expediente')
def Expediente():
    expedientesSinDatos = pruebaBD.expedientesSinDatos()
    return render_template('expedientessindatos.html',expedientes = expedientesSinDatos)


@app.route('/explist')
def IndexExp():
    expedientesubic = pruebaBD.selectExpedientesUbicacion('DESPACHO',1)
    return render_template('expedientesList.html',expedientes = expedientesubic)

@app.route('/movimientosexp/<idexp>')
def MoxExp(idexp):
    expedientesubic = pruebaBD.selectMovimientoExpediente(idexp)
    return render_template('ultimomovimientosexp.html',expedientes = expedientesubic)

@app.route('/expedientes-sarg')
def ExpedientesSarg():
    expedientesProcurador = pruebaBD.selecExpedientesProcurador(2)
    return render_template('expedientessargiotto.html',procuradores = procuradores1[1],expedientes = expedientesProcurador)

@app.route('/expedientes-colom')
def ExpedientesColom():
    expedientesProcurador = pruebaBD.selecExpedientesProcurador(1)
    return render_template('expedientescolombero.html',procuradores = procuradores1[0],expedientes = expedientesProcurador)

@app.route('/expedientes-bruna')
def ExpedientesBruna():
    expedientesProcurador = pruebaBD.selecExpedientesProcurador(3)
    return render_template('expedientesbruna.html',procuradores = procuradores1[2],expedientes = expedientesProcurador)

if __name__ == '__main__':
    app.run(host="127.0.0.1",port=8080,debug=True)
