import pymysql
############### CONFIGURAR ESTO ###################
# Abre conexion con la base de datos
# db = pymysql.connect("35.199.108.216","sacadmin","penka","sac")
##################################################

# prepare a cursor object using cursor() method
# cursor = db.cursor()

# ejecuta el SQL query usando el metodo execute().
# cursor.execute("SELECT VERSION()")

##################################################################
#### SELECT EXPEDIENTES TODOS                                #####
##################################################################
def selectTodosExpedientes():
    db = pymysql.connect("35.199.108.216","sacadmin","penka","sac")
    ##################################################

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # ejecuta el SQL query usando el metodo execute().
    cursor.execute("SELECT VERSION()")
    data = cursor.fetchone()
    data = cursor.fetchone()
    sql = "SELECT * FROM expedientes"
    cursor.execute(sql)

    # Fetch all the rows in a list of lists.
    results = cursor.fetchall()
    for row in results:
       # Now print fetched result
       print(row)

    # disconnect from server
    db.close()
    return results

##################################################################
#### SELECT EXPEDIENTES  POR ID                              #####
##################################################################
def selectExpedientes(idexp):
    db = pymysql.connect("35.199.108.216","sacadmin","penka","sac")
    ##################################################

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # ejecuta el SQL query usando el metodo execute().
    cursor.execute("SELECT VERSION()")
    data = cursor.fetchone()
    sql = "SELECT * FROM expedientes WHERE idexpediente = {0}".format(idexp)
    cursor.execute(sql)

    # Fetch all the rows in a list of lists.
    results = cursor.fetchall()
    for row in results:
       # Now print fetched result
       print(row)

    # disconnect from server
    db.close()
    return results


def selecProcurador(idproc = None):
    db = pymysql.connect("35.199.108.216","sacadmin","penka","sac")
    ##################################################

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # ejecuta el SQL query usando el metodo execute().
    cursor.execute("SELECT VERSION()")

    if idproc is None:
        sql = "SELECT * FROM procuradores"
    else:
        sql = "SELECT * FROM procuradores WHERE idprocurador = {0}".format(idproc)
    cursor.execute(sql)

    # Fetch all the rows in a list of lists.
    results = cursor.fetchall()
    for row in results:
       # Now print fetched result
       print(row)

    # disconnect from server
    db.close()
    return results

##################################################################
#### SELECT EXPEDIENTES  POR PROCURADOR                      #####
##################################################################
def selecExpedientesProcurador(idproc):
    db = pymysql.connect("35.199.108.216","sacadmin","penka","sac")
    ##################################################

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # ejecuta el SQL query usando el metodo execute().
    cursor.execute("SELECT VERSION()")
    data = cursor.fetchone()
    sql = "SELECT * FROM expedientes WHERE idprocurador = {0} and caratula is not null".format(idproc)
    cursor.execute(sql)

    # Fetch all the rows in a list of lists.
    results = cursor.fetchall()
    for row in results:
       # Now print fetched result
       print(row)

    # disconnect from server
    db.close()
    return results

##################################################################
#### SELECT MOVIMIENTOS  POR EXPEDIENTE                      #####
##################################################################
def selectMovimientoExpediente(idexp):
    db = pymysql.connect("35.199.108.216","sacadmin","penka","sac")
    ##################################################

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # ejecuta el SQL query usando el metodo execute().
    cursor.execute("SELECT VERSION()")
    data = cursor.fetchone()
    sql = "SELECT * FROM sac.movimientos WHERE idexpediente = {0}".format(idexp)
    cursor.execute(sql)

    # Fetch all the rows in a list of lists.
    results = cursor.fetchall()
    for row in results:
       # Now print fetched result
       print(row)

    # disconnect from server
    db.close()
    return results

##################################################################
#### SELECT MOVIMIENTOS POR PROCURADOR                       #####
##################################################################
def selectMovimientoPorProcurador(idproc):
    db = pymysql.connect("35.199.108.216","sacadmin","penka","sac")
    ##################################################

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # ejecuta el SQL query usando el metodo execute().
    cursor.execute("SELECT VERSION()")
    data = cursor.fetchone()
    sql = "SELECT * FROM movimientos m INNER JOIN expedientes e ON e.idexpediente = m.idexpediente WHERE idprocurador = {0} LIMIT 0,100000".format(idproc)
    cursor.execute(sql)

    # Fetch all the rows in a list of lists.
    results = cursor.fetchall()
    for row in results:
       # Now print fetched result
       print(row)
       # input()

    # disconnect from server
    db.close()
    return results

##################################################################
#### SELECT MOVIMIENTOS POR OPERACION                        #####
##################################################################
def selectMovimientoPorOperacion(operacion):
    db = pymysql.connect("35.199.108.216","sacadmin","penka","sac")
    ##################################################

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # ejecuta el SQL query usando el metodo execute().
    cursor.execute("SELECT VERSION()")
    data = cursor.fetchone()
    sql = "SELECT * FROM sac.movimientos where operacion LIKE '%{0}%'".format(operacion)
    cursor.execute(sql)

    # Fetch all the rows in a list of lists.
    results = cursor.fetchall()
    for row in results:
       # Now print fetched result
       print(row)

    # disconnect from server
    db.close()
    return results

##################################################################
#### SELECT MOVIMIENTOS  POR EXPEDIENTE Y PROCURADOR         #####
##################################################################
def selectMovimientoProcuradorExpediente(idexp,idproc):
    db = pymysql.connect("35.199.108.216","sacadmin","penka","sac")
    ##################################################

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # ejecuta el SQL query usando el metodo execute().
    cursor.execute("SELECT VERSION()")
    data = cursor.fetchone()
    sql = "SELECT * FROM movimientos m INNER JOIN expedientes e ON e.idexpediente = m.idexpediente WHERE idprocurador = {0} AND m.idexpediente = {1}".format(idproc,idexp)
    cursor.execute(sql)

    # Fetch all the rows in a list of lists.
    results = cursor.fetchall()
    for row in results:
       # Now print fetched result
       print(row)

    # disconnect from server
    db.close()
    return results

##################################################################
#### SELECT MOVIMIENTOS POR Operacion y Fecha                #####
##################################################################
def selectMovimientoPorOperacionYFecha(operacion,fecha):
    db = pymysql.connect("35.199.108.216","sacadmin","penka","sac")
    ##################################################

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # ejecuta el SQL query usando el metodo execute().
    cursor.execute("SELECT VERSION()")
    data = cursor.fetchone()
    sql = "SELECT * FROM sac.movimientos where operacion = {0} and fecha_mov = '{1}';".format(operacion,fecha)
    cursor.execute(sql)

    # Fetch all the rows in a list of lists.
    results = cursor.fetchall()
    for row in results:
       # Now print fetched result
       print(row)

    # disconnect from server
    db.close()
    return results

##################################################################
#### SELECT expedientes sin datos                            #####
##################################################################
def expedientesSinDatos():
    db = pymysql.connect("35.199.108.216","sacadmin","penka","sac")
    ##################################################

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # ejecuta el SQL query usando el metodo execute().
    cursor.execute("SELECT VERSION()")
    data = cursor.fetchone()
    sql = "SELECT * FROM expedientes e left join procuradores p on p.idprocurador = e.idprocurador where e.caratula is null;"
    cursor.execute(sql)

    # Fetch all the rows in a list of lists.
    results = cursor.fetchall()
    for row in results:
       # Now print fetched result
       print(row)

    # disconnect from server
    db.close()
    return results


##################################################################
#### SELECT Expedientes Ubicacion                             #####
##################################################################
def selectExpedientesUbicacion(ubicacion,idproc = None):
    db = pymysql.connect("35.199.108.216","sacadmin","penka","sac")
    ##################################################

    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # ejecuta el SQL query usando el metodo execute().
    cursor.execute("SELECT VERSION()")
    data = cursor.fetchone()
    if idproc is None:
        sql = "SELECT * FROM expedientes where ubicacion LIKE '%{0}%'".format(ubicacion)
    else:
        sql = "SELECT * FROM expedientes e inner join procuradores p on e.idprocurador = p.idprocurador where ubicacion LIKE '%{0}%' and e.idprocurador = {1}".format(ubicacion,idproc)
    
    cursor.execute(sql)

    # Fetch all the rows in a list of lists.
    results = cursor.fetchall()
    for row in results:
       # Now print fetched result
       print(row)

    # disconnect from server
    db.close()
    return results

